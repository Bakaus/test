// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.

function formatDate(userDate) {
  const date = new Date(userDate);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const addZero = (time) =>
    String(time).length === 1 ? `0${time}` : `${time}`; //Função que verifica se necessita adicionar 0 no numero
  return `${year}${addZero(month)}${addZero(day)}`;
}

console.log(formatDate('12/31/2014'));
console.log(formatDate('01/01/2014'));
console.log(formatDate('03/26/1998'));
console.log(formatDate('03/30/2000'));
console.log(formatDate('12/23/1963'));
