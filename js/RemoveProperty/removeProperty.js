// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  if (obj[prop] === undefined) return false; //caso nao tenha a prop
  delete obj[prop]; //deletando a prop do obj
  return true;
}

//obj teste
const player = {
  name: 'Bakarib4',
  age: '22',
};
console.log(player);
console.log(removeProperty(player, 'name'));
console.log(player);
console.log(removeProperty(player, 'age'));
console.log(player);
